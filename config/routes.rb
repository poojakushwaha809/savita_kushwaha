Rails.application.routes.draw do
  devise_for :admins

 
  get 'homes/index'
  get 'homes/test', to: 'homes#test'

  root 'homes#index'
   resources :menus do
    resources :dishes do
    end
  end
  
  resources :special_dishes 
  resources :galleries 
  resources :users 
  resources :abouts 

  post 'dishes/:dish_id/dish_images', to: 'dishes#add_image', as: :dish_images
  # get 'dishes/:dish_id/dish_images/:id', to: 'dishes#image_show'
  get 'dishes/:dish_id/dish_images/:id', to: 'dishes#image_destroy', as: :dish_image_destroy

  post 'dish/images', to: 'homes#dish_images', as: :popup_dish_images

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
