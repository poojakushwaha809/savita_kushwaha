class CreateDishImages < ActiveRecord::Migration[5.2]
  def change
    create_table :dish_images do |t|
      t.string :dish_image
      t.integer :dish_id

      t.timestamps
    end
  end
end
