class CreateSpecialDishes < ActiveRecord::Migration[5.2]
  def change
    create_table :special_dishes do |t|
      t.string :name
      t.string :image
      t.string :description
      t.string :heading

      t.timestamps
    end
  end
end
