class SpecialDish < ApplicationRecord
 mount_uploader :image, SpecialDishImageUploader
 validates :name,:heading,:description, presence: true, uniqueness: :true

end
