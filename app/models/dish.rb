class Dish < ApplicationRecord
validates :name, :price, :presence => true

	belongs_to :menu
	has_many :dish_images, dependent: :destroy

end
