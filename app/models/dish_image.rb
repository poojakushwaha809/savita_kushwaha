class DishImage < ApplicationRecord
	mount_uploader :dish_image, DishImageUploader
	belongs_to :dish
end
