class Gallery < ApplicationRecord
   mount_uploader :gallery_image, GalleryUploader
 validates :gallery_image, :presence => true

end
