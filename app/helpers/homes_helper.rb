module HomesHelper
	def get_transparent_topbar_class
		 get_condition ?   "topbar-transparent" : ""
	end

	def get_transparent_header_class
		get_condition ?   "header-transparent" : ""
	end

	def get_condition
		return params[:controller] == 'homes' && params[:action] == 'index'
	end
end
