class UserMailer < ApplicationMailer
	  def new_user_email
    @user = params[:user]
    mail(to: @user.email, subject: "Thanks for order!")
  end

  def admin_email
  	@user = params[:user]
    mail(to: "poojakushwaha809@gmail.com", subject: "You got order from #{@user.name}!")
  end
end
