class ApplicationMailer < ActionMailer::Base
  default from: 'poojakushwaha809@gmail.com'
  layout 'mailer'
end
