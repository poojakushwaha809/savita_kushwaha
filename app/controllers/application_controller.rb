class ApplicationController < ActionController::Base
	  protect_from_forgery with: :null_session
	  before_action :get_about

	  def get_about
       @about = About.first	  	
	  end

end
