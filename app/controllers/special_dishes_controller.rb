class SpecialDishesController < ApplicationController
   before_action :authenticate_admin! 

  def index
    @special_dishes = SpecialDish.all
  end
 
  def show
    @special_dish = SpecialDish.find(params[:id])
  end
 
  def new
    @special_dish = SpecialDish.new
  end
 
  def edit
    @special_dish = SpecialDish.find(params[:id])
  end
 
  def create
    @special_dish = SpecialDish.new(special_dish_params)
    
    if @special_dish.save

      redirect_to special_dishes_path
    else
      render 'new'
    end
  end
 
  def update
    @special_dish = SpecialDish.find(params[:id])
 
    if @special_dish.update(special_dish_params)
      redirect_to special_dishes_path
    else
      render 'edit'
    end
  end
 
  def destroy
    @special_dish = SpecialDish.find(params[:id])
    @special_dish.destroy
 
    redirect_to special_dishes_path
  end
 
  private
    def special_dish_params
      params.require(:special_dish).permit(:name, :image, :heading, :description)
    end
end
