class DishesController < ApplicationController
    before_action :authenticate_admin! 

 def new
   @menu = Menu.find(params[:menu_id])
   @dish = Dish.new

 end

 def show
    @menu = Menu.find(params[:menu_id])
    @dish = Dish.find(params[:id])
 end

  def create
    @menu = Menu.find(params[:menu_id])
    @dish = @menu.dishes.new(dish_params)
    if @dish.save
    redirect_to menu_path(@menu)
    else
     render 'new'
    end
  end
  def destroy
    @menu = Menu.find(params[:menu_id])
    @dish = @menu.dishes.find(params[:id])
    @dish.destroy
    redirect_to menu_path(@menu)
  end

  def add_image
   # byebug
    @dish = Dish.find(params[:dish_id])
    @dish_image = @dish.dish_images.create(dish_image: params[:dish_image][:dish_image])
    redirect_to menu_dish_path(@dish.menu,@dish)
  end

  # def image_show
   
  # end

  def image_destroy 
  # byebug
    @dish = Dish.find(params[:dish_id])
    @dish_image = @dish.dish_images.find(params[:id])
    @dish_image.destroy
    redirect_to menu_dish_path(@dish.menu,@dish)

  end  

  private
    def dish_params
      params.require(:dish).permit(:name, :description, :price)
    end

end
